package com.amazing.app

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * Volley adapter for JSON requests that will be parsed into Java objects by
 * Gson.
 */
public class GsonRequest<T> extends Request<T> {
	private static final String TAG = GsonRequest.class.getSimpleName();
	private final Gson mGson;
	private final Map<String, String> mHeaders;
	private final Listener<T> mListener;
	private Type mType;

	/**
	 * Make a GET request and return a parsed object from JSON.
	 *
	 * @param url     URL of the request to make
	 * @param type    The type for Gson's reflection. e.g. new TypeToken<List<Model>>(){}.getType()
	 * @param headers Map of request headers
	 */
	public GsonRequest(String url, Type type, Map<String, String> headers,
					   Listener<T> listener, ErrorListener errorListener) {
		super(Method.GET, url, errorListener);
		mGson = new GsonBuilder().enableComplexMapKeySerialization().create();
		mHeaders = headers;
		mListener = listener;
		mType = type;
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		return mHeaders != null ? mHeaders : super.getHeaders();
	}

	@Override
	protected void deliverResponse(T response) {
		mListener.onResponse(response);
	}

	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
		try {
			String json = new String(networkResponse.data,
					HttpHeaderParser.parseCharset(networkResponse.headers));

			T parsedResponse = mGson.fromJson(json, mType);

			return Response.success(parsedResponse,
					HttpHeaderParser.parseCacheHeaders(networkResponse));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JsonSyntaxException e) {
			return Response.error(new ParseError(e));
		}
	}
}
